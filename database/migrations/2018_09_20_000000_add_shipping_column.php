<?php

use Illuminate\Database\Migrations\Migration;

class AddShippingColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function ($table) {
            $table->text('shipping')->nullable();
            $table->text('terms_of_payment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function ($table) {
            $table->dropColumn('shipping');
            $table->dropColumn('terms_of_payment');
        });
    }
}
